const conn = new Mongo();
const db = conn.getDB("deedpicker");

const activitiesMeta = db.runCommand({"listCollections": 1, filter: {name: "activities" }});
const daysMeta = db.runCommand({"listCollections": 1, filter: {name: "days" }});

const initDb = () => {
  db.createCollection("activities");
  db.activities.createIndex({name: 1}, {unique: 1});
  const testActivity = {
    createdAt: new Date(),
    updatedAt: new Date(),
    name: "Test activity",
  };
  const insertedDocument = db.activities.insertOne(testActivity);

  const activityId = insertedDocument.insertedId + ''; // otherwise it'll be 'ObjectId('...')'
  const date = new Date();
  date.setHours(0, 0, 0, 0);
  const testDay = {
    createdAt: new Date(),
    updatedAt: new Date(),
    date,
    activityStatuses: {
      [activityId]: false,
    },
  };
  db.createCollection("days");
  db.days.createIndex({date: 1}, {unique: 1});
  db.days.insert(testDay);

  db.dummyData.drop();
}

if (!activitiesMeta.cursor.firstBatch.length && !daysMeta.cursor.firstBatch.length) {
  initDb();
}
