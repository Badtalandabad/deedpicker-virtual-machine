class nodejspack {
  class { 'nodejs':
    repo_url_suffix => '12.x',
  }

  package { 'pm2':
    ensure   => 'present',
    provider => 'npm',
  }
}
