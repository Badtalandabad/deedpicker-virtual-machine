class nginx {
  package { 'nginx':
    ensure => 'present',
    require => Exec['apt-get update'],
  }

  # Hosts
  file { 'nginx-default-host':
    path => '/etc/nginx/sites-available/default',
    ensure => file,
    replace => true,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/default',
  }
  
  file { 'deedpicker-host':
    path => '/etc/nginx/sites-available/deedpicker.local',
    ensure => file,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/deedpicker.local',
  }
  file { 'deedpicker-host-enabled':
    path => '/etc/nginx/sites-enabled/deedpicker.local',
    target => '/etc/nginx/sites-available/deedpicker.local',
    ensure => link,
    #notify => Service['nginx'],
    require => File['deedpicker-host'],
  }

  file { 'deedpicker-cert':
    path => '/etc/nginx/ssl',
    ensure => 'directory',
    recurse => 'remote',
    source => 'puppet:///modules/nginx/ssl',
    require => Package['nginx'],
  }

  service { 'nginx':
    ensure => running,
    require => Package['nginx'],
  }
}